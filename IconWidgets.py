__author__ = 'iwelch'
from PyQt4 import QtGui
import PyQt4.QtCore as QtCore
from PyQt4.QtGui import *
"""Create Qlabel classes that allow widgets to be added in teh UI
"""

class MovWidget(QtGui.QLabel):

    def __init__(self,imagePath, parent=None):
        super(MovWidget, self).__init__(parent)

        movie = QtGui.QMovie(imagePath)
        self.setAlignment(QtCore.Qt.AlignCenter)
        self.setMovie(movie)
        movie.start()


class ImgWidget(QtGui.QLabel):

    def __init__(self, imagePath, parent=None):
        super(ImgWidget, self).__init__(parent)

        pic = QtGui.QPixmap(imagePath)
        smallpic = pic.scaledToWidth(20)

        self.setAlignment(QtCore.Qt.AlignCenter)
        self.setPixmap(smallpic)
