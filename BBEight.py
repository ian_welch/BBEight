import argparse
import glob
import os
import shutil
import sys
import time

from PyQt4 import QtGui
from PyQt4.QtCore import *
from PyQt4.QtCore import QThread, SIGNAL
from PyQt4.QtGui import *
from robot import rebot
from robot.api import TestSuiteBuilder

import IconWidgets
import design
import logging as logger

__author__ = 'iwelch'

"""
BB8 or BBEight as the repository is called is a UI wrapper around RobotFramework
that is used as a read only interface for running testing in the factory or RFC

"""


class RobotListener(QThread):
    """ This class acts as a listener thread that is registered to RobotFramework at run time. By registering to the
    RobotFramework Listener API we can get real time updates on teh status of tests. Note, these operations are blocking
    """
    ROBOT_LISTENER_API_VERSION = 3

    def __init__(self):
        logger.debug("Creating a new instance of Robot Listener")
        QThread.__init__(self)
        self.tests = []
        logger.basicConfig(level=logger.DEBUG)

    def end_suite(self, name, attributes):
        logger.debug("Suite complete")
        for foo in self.tests:
            print foo.name
            for result in foo.results:
                print ("\t{}\t{}\t{}".format(result['result'], result['execution_time'], result['duration']))
        self.emit(SIGNAL('suite_ended(QString)'), 'Suite Ended')

    def start_suite(self, name, attributes):
        logger.debug("Staring the test suite")
        self.emit(SIGNAL('suite_started(QString)'), 'Suite Started')

    def start_test(self, data, result):
        logger.debug("Test: %s started", result.name)
        for test in self.tests:
            if test.id == data.id:
                test.status = 'RUNNING'
        self.emit(SIGNAL('test_running(QString)'), 'Tests loaded')

    def end_test(self, data, result):
        logger.debug("Test: %s complete\tID: %s\tResult:%s", result.name, result.elapsedtime, result.status)
        for test in self.tests:
            if test.id == data.id:
                test.status = result.status
                test.add_result(result.status, result.starttime, result.elapsedtime)
        self.emit(SIGNAL('test_complete(QString)'), 'Test complete')
        #print 'Test: %s ran in %s ms and resulted in a %s' % (result.name, result.elapsedtime, result.status)

    def log_message(self, message):
        logger.debug("Message %s: `%s`", message.level, message.message)
        
    def load_suite(self, suite):
        logger.debug("Loading test suite")
        for test in suite.tests:
            self.tests.append(TestContainer(test.name, test.id))
            logger.debug("Loaded test - %s with id - % s", test.name, test.id)
        self.emit(SIGNAL('suite_loaded(QString)'), 'Suite loaded')
        
    # Update the GUI's running loop counter    
    def update_guiLoopCnt(self, loopCnt):
        self.emit(SIGNAL('update_loop_cnt(QString)'), loopCnt)


class TestContainer:
    # TODO should this just inherit a test object from RobotFramework and add additional variables / methods?
    def __init__(self, name, id):
        self.name = name
        self.id = id
        self.status = 'NOT_RUN'
        self.results = []
        pass

    def add_result(self, result, time, duration):
        my_result = {'result': result, 'execution_time': time, 'duration': duration}
        self.results.append(my_result)

    def get_results(self):
        return self.results


class ExampleApp(QtGui.QMainWindow, design.Ui_MainWindow):
    def __init__(self, parent=None):
        super(ExampleApp, self).__init__(parent)
        self.setupUi(self)
        #Wire up methods to respond to UI actions
        self.btnStart.clicked.connect(self.start_test)
        self.btnLoad.clicked.connect(self.load_test)
        self.btnAbort.clicked.connect(self.abort_suite)
        self.tableWidget.setHorizontalHeaderLabels(['Test Name',
                                                    'Test Result',
                                                    'Time',
                                                    'Duration(ms)'])
        # Create a new thread that will be registered to RobotFramework as a listener
        self.listen_thread = RobotListener()
        self.connect(self.listen_thread, SIGNAL("suite_loaded(QString)"), self.populate_test_table)
        #self.connect(self.listen_thread, SIGNAL("suite_ended(QString)"), self.end_suite)
        logger.basicConfig(level=logger.DEBUG)

    def abort_suite(self):
        self.get_thread.stop()

    def start_test(self):
        # Create a new thread to execute the RobotFramework test and register it to RobotFramework
        self.get_thread = executeRobotThread(self.listen_thread, self.test_suite_file, self.sldrTestRepeat.value())
        self.connect(self.get_thread, SIGNAL("finished()"), self.done)

        #Start Robot framework. Callbacks from Robot framework should be serviced in the Listener thread not the
        #RobotFramework execution thread
        self.connect(self.listen_thread, SIGNAL("test_complete(QString)"), self.update_test_table)
        self.connect(self.listen_thread, SIGNAL('test_running(QString)'), self.update_test_table)
        self.connect(self.listen_thread, SIGNAL("update_loop_cnt(QString)"), self.update_runloop_cnt)
        self.btnLoad.setEnabled(False)
        self.btnStart.setEnabled(False)
        self.sldrTestRepeat.setEnabled(False)

        self.get_thread.start()

    def done(self):
        self.btnLoad.setEnabled(True)
        self.btnStart.setEnabled(True)
        self.sldrTestRepeat.setEnabled(True)

    def load_test(self):
        self.test_suite_file = QtGui.QFileDialog.getOpenFileName(self, "Pick a test suite")
        self.labelLoadedPath.setText(self.test_suite_file)
        self.labelLoadedPath.update()
        suite = TestSuiteBuilder().build(str(self.test_suite_file))
        self.listen_thread.load_suite(suite)

    def populate_test_table(self, name):
        # TODO should this be passed in a QObject instead of access the class instance directly?
        i = 0
        for test in self.listen_thread.tests:
            self.tableWidget.insertRow(i+1)
            self.tableWidget.setItem(i, 0, QTableWidgetItem(test.name))
            i += 1

    def update_test_table(self, name):
        # TODO should this be passed in a QObject instead of access the class instance directly?
        i = 0
        for test in self.listen_thread.tests:
            # First update the latest result
            self.tableWidget.setItem(i, 0, QTableWidgetItem(test.name))
            if test.status == 'RUNNING':
                self.tableWidget.setCellWidget(i, 1, IconWidgets.MovWidget('icons/loader.gif'))
            elif test.status == 'PASS':
                self.tableWidget.setCellWidget(i, 1, IconWidgets.ImgWidget('icons/checkmark.png'))
            elif test.status == 'FAIL':
                self.tableWidget.setCellWidget(i, 1, IconWidgets.ImgWidget('icons/close.png'))
            j = 1
            #Then recurs the previous results
            for result in test.results:
                self.tableWidget.setItem(i, 2, QTableWidgetItem(result['execution_time']))
                self.tableWidget.setItem(i, 3, QTableWidgetItem(str(result['duration'])))
                j += 1
            i += 1
            
    def update_runloop_cnt(self, loopCnt):
        self.runLoopCnt.setText(loopCnt)
        self.runLoopCnt.update()


class executeRobotThread(QThread):
    ROBOT_LISTENER_API_VERSION = 3

    def __init__(self, listener, suite_path, loop):
        QThread.__init__(self)
        self.listener = listener
        self.suite_path = suite_path
        self.loop = loop
        self.abort = False
        logger.basicConfig(level=logger.DEBUG)

    def __del__(self):
        self.wait()

    def stop(self):
        logger.debug("Aborting test suite")
        self.abort = True
        
    def run(self):
        self.suite = TestSuiteBuilder().build(str(self.suite_path))

        self.logDir = 'log/' + time.strftime("%Y%m%d_%H%M%S")
        self.logDirXml = self.logDir + "/xml"
        try:
            os.makedirs(self.logDir) # create base destination directory
            os.makedirs(self.logDirXml) # destination directory for output files
        except OSError:
            # The directory already existed, nothing to do
            pass
        
        loopCnt = 0 
        while True:
            if self.abort:
                logger.debug("Test suite aborted")
                break
            self.listener.update_guiLoopCnt(str(loopCnt + 1)) # update running loop counter
            file_name = "run-" + str(loopCnt+1).rjust(6, '0') 
            # self.suite.run is equivilent to command line "pybot --param1 --param2 --etc"
            result = self.suite.run(listener=self.listener, console='quiet', output=file_name, outputdir=self.logDirXml, variable="LOG_DIR:"+self.logDir, loglevel="DEBUG")
            
            loopCnt += 1
            if self.loop > 0:   # If 0 then loop forever
                if loopCnt == self.loop:
                    break
        
        #TODO this should use a robot libraryrenew
        
        # Rebot builds the report in random output file order so order the file
        # list before passing into the call
        os.system("python -m robot.rebot --name CombinedResults " + self.logDirXml + "/*.xml")
        
        for filename in glob.iglob('*.html'): # Copy result files to log directory
            shutil.copy2(filename, self.logDir)
            
        for filename in glob.iglob('*.log'): # Copy result files to log directory
            shutil.copy2(filename, self.logDir)

def main():
    logger.basicConfig(level=logger.DEBUG,
                           format='%(asctime)s %(levelname)s %(message)s',
                           filename='BBEight.log',
                           filemode='w')
    parser = argparse.ArgumentParser(description='Used to continuously run RobotFramework test suites')
    parser.add_argument('suite_file', help='RobotFramework test suite file')
    parser.add_argument('loop_count', type=int, help='Number of time to loop the test suite file')
    parser.add_argument('--interactive', help='Run BBEight in UI mode', action='store_true')
    args = parser.parse_args()

    app = QtGui.QApplication(sys.argv)
    form = ExampleApp()
    form.show()
    app.exec_()

    # myListener = RobotListener()
    #suite = TestSuiteBuilder().build(args.suite_file) #'tests/basic_test.robot'
    #for i in range(0, args.loop_count):
    #    result = suite.run(listener=myListener, console='quiet')

if __name__ == '__main__':
    main()