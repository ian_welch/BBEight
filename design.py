# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'design.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(708, 543)
        MainWindow.setInputMethodHints(QtCore.Qt.ImhDigitsOnly)
        MainWindow.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        MainWindow.setTabShape(QtGui.QTabWidget.Triangular)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setBaseSize(QtCore.QSize(5, 2))
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.btnLoad = QtGui.QPushButton(self.centralwidget)
        self.btnLoad.setObjectName(_fromUtf8("btnLoad"))
        self.verticalLayout.addWidget(self.btnLoad)
        self.btnStart = QtGui.QPushButton(self.centralwidget)
        self.btnStart.setObjectName(_fromUtf8("btnStart"))
        self.verticalLayout.addWidget(self.btnStart)
        self.btnAbort = QtGui.QPushButton(self.centralwidget)
        self.btnAbort.setObjectName(_fromUtf8("btnAbort"))
        self.verticalLayout.addWidget(self.btnAbort)
        spacerItem = QtGui.QSpacerItem(20, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.sldrTestRepeat = QtGui.QSpinBox(self.centralwidget)
        self.sldrTestRepeat.setMaximumSize(QtCore.QSize(64, 16777215))
        self.sldrTestRepeat.setMinimum(0)
        self.sldrTestRepeat.setMaximum(9999)
        self.sldrTestRepeat.setValue(1)  # Default value
        self.sldrTestRepeat.setObjectName(_fromUtf8("sldrTestRepeat"))
        self.horizontalLayout.addWidget(self.sldrTestRepeat)
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        
        #Loop count widgets
        self.runLooplabel = QtGui.QLabel("     Running Loop:")
        self.runLooplabel.setObjectName(_fromUtf8("runLooplabel"))
        self.horizontalLayout.addWidget(self.runLooplabel)
        self.runLoopCnt = QtGui.QLabel("0")
        self.runLoopCnt.setObjectName(_fromUtf8("runLoopCnt"))
        self.horizontalLayout.addWidget(self.runLoopCnt)
        self.horizontalLayout.addStretch(1)
        
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.labelLoadedPath = QtGui.QLabel(self.centralwidget)
        self.labelLoadedPath.setObjectName(_fromUtf8("labelLoadedPath"))
        self.verticalLayout.addWidget(self.labelLoadedPath)
        self.tableWidget = QtGui.QTableWidget(self.centralwidget)
        self.tableWidget.setMinimumSize(QtCore.QSize(0, 0))
        self.tableWidget.setSizeIncrement(QtCore.QSize(0, 0))
        self.tableWidget.setBaseSize(QtCore.QSize(0, 0))
        self.tableWidget.setRowCount(1)
        self.tableWidget.setColumnCount(4)
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.horizontalHeader().setVisible(True)
        self.tableWidget.horizontalHeader().setCascadingSectionResizes(False)
        self.tableWidget.horizontalHeader().setSortIndicatorShown(False)
        self.tableWidget.horizontalHeader().setStretchLastSection(False)
        self.tableWidget.verticalHeader().setVisible(False)
        self.verticalLayout.addWidget(self.tableWidget)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 708, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Trimble - Robot Test Runner", None))
        MainWindow.setWindowIcon(QtGui.QIcon("icons/trimble.gif"))
        self.btnLoad.setText(_translate("MainWindow", "Load Test Suite", None))
        self.btnStart.setText(_translate("MainWindow", "Start Test", None))
        self.btnAbort.setText(_translate("MainWindow", "Abort Suite", None))
        self.label.setText(_translate("MainWindow", "Repeat Test", None))
        self.labelLoadedPath.setText(_translate("MainWindow", "TextLabel", None))

