# Copyright (c) 2017, Trimble Inc.. All rights reserved.

"""
Generates Functional Robot Report.html and Log.html from robot results .xml
files. This is useful when a looping robot test is still running and the user
wants to check the current results of the test.

Author: tyork
Created: 01/23/2017
"""

import getopt
import glob
import os
import shutil
import sys
import time
from xml.sax import make_parser
from xml.sax.handler import ContentHandler

# Globals
m_PATHSEP = os.path.sep  # File path separator
m_REPORT_NAME = "CombinedResults"  # Name to give generated reports
m_LOG_DIR = "xml"  # Directory were .xml files are stored
m_LOG_FILES_PATH = m_LOG_DIR + m_PATHSEP + "*.xml" 
m_TMP_LOG_DIR_NAME = "tmpxml" # Directory where xml files are temporarily stored and processed

m_logDirPath = ''  # Command line parameter pointing to base directory of log files
m_tmpLogDir = ''  # XML copied here. BBEight doesn't like work on originals while tests are running

def parse_params(argv):
    """ Parse the command line arguments and validate the input as needed. """
    
    global m_TMP_LOG_DIR_NAME
    global m_logDirPath
    global m_tmpLogDir
    
    USAGE_STR = "Usage: robotReportGen.py -i <xmlPath>"
    
    found_i = False  # Enforce mandatory parameters
    
    try:
        opts, args = getopt.getopt(argv, "hi:", ["ifile="])
    except getopt.GetoptError:
        print(USAGE_STR)
        sys.exit(2)
    
    for opt, arg in opts:
        if opt == '-h':
            print(USAGE_STR)
            sys.exit()
        elif opt in ("-i", "--ifile"):
            found_i = True
            m_logDirPath = arg
            m_tmpLogDir = m_logDirPath + m_PATHSEP + m_TMP_LOG_DIR_NAME
     
    if not found_i:
        print("Missing -i parameter") 
        print(USAGE_STR)
        sys.exit(2) 

def checkIfFileExists(filepath):
    """ Check if a file or files exist.
    
        Args:
            filepath (str): Path including filename or wildcard.
        
        Returns:
            True if the file if one or more files matching filepath are found,
            False otherwise.
    """
         
    for filepath_object in glob.glob(filepath):
        if os.path.isfile(filepath_object):
            return True
    return False   
    
def cleanUp():
    """ Cleanup all temporary files and directories created by this program. """
    
    global m_tmpLogDir
    print("***Cleaning up temporary files")
    if os.path.exists(m_tmpLogDir):
        shutil.rmtree(m_tmpLogDir)
    time.sleep(1)
        
def createTmpDataDir():
    """ Creates a temporary directory to store copies the xml results files. 
        
        This is needed because BBEight doesn't like it when the robot.rebot 
        tool is ran while tests are in progress. Running robot.rebot while
        tests are active may cause a suite loop to fail. 
    """
    global m_LOG_DIR
    global m_tmpLogDir
    global m_logDirPath
    
    if not os.path.exists(m_tmpLogDir):
        os.makedirs(m_tmpLogDir)
      
    if not checkIfFileExists(m_logDirPath + m_PATHSEP + m_LOG_DIR + m_PATHSEP + '*.xml'):
        raise IOError("File not found")
        
    for filename in glob.glob(os.path.join(m_logDirPath + m_PATHSEP + m_LOG_DIR, '*.xml')):
        shutil.copy2(filename, m_tmpLogDir)

def removeInvalidXmlFile():
    """ Remove the last created xml results file if it is malformed. """
    
    global m_tmpLogDir
    print("***Validating xml")
    files = glob.glob(m_tmpLogDir + m_PATHSEP + "*.xml")
    newestFile = max(files, key=os.path.getmtime)
    if not validateXml(newestFile):
        os.remove(newestFile)

def validateXml(fileToValidate):
    """ Validate the formatedness of the xml fileToValidate passed in.
        
        Args:
            fileToValidate (str): The name of the fileToValidate to validate.
        
        Returns:
            True if the fileToValidate is well formed, False otherwise. 
    """
    try:
        # Check if file is good XML
        parser = make_parser()
        parser.setContentHandler(ContentHandler())
        parser.parse(fileToValidate)
        
        # Check if file has closing tag </robot> if not then this file is not completed run
        print(fileToValidate)
        with open(fileToValidate) as f:
            found = False
            for line in f:
                if '</robot>' in line: # Key line: check if `w` is in the line.
                    found = True 
            if not found:
                raise Exception('File missing </robot> tag')
        return True
    except Exception, e:
        print("\"%s\" is NOT well-formed! %s" % (fileToValidate, e))
        print("Removing \"" + fileToValidate + "\" from report")
        return False
        
def main(argv):
    """ Program entry point. 
    
        This program assumes the robot xml results files (*.xml) are stored in a 
        directory called xml.
        
        Usage: 
           python RobotReportGen.py -i <pathToLogs>
              where: pathToLogs is the parent directory of the xml directory 
                     where the robot xml results files are stored.
    """
    parse_params(argv) 
    
    # Setup
    try:
        cleanUp()  # make sure there is nothing left over from prior run
        createTmpDataDir()
        removeInvalidXmlFile() 
    except IOError, e:
        print("ERROR: No valid files found to process. Check path: \"" + argv[1] + "\"")
        cleanUp()
        sys.exit(2)
                
    # Generate reports  
    print("***Generating Reports")  
    os.system("python -m robot.rebot --name " + m_REPORT_NAME 
              + " --outputdir " + m_logDirPath + " " + m_tmpLogDir + m_PATHSEP + "*.xml")
    
    cleanUp()
    print("***Completed")
    
if __name__ == "__main__":
    main(sys.argv[1:])
