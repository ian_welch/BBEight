*** Settings ***
Library           OperatingSystem
Library           Process

*** Test Cases ***
Ping Test Fail
    [Documentation]    This is a simple test to show how Robot can be used to run command line argumets from the OS
    [Tags]    demo
    ${RESULT}    Run    ping www.google.foo
    ShouldContain    ${RESULT}    0%
    set suite metadata  ping_result    ${RESULT}

Ping Test Pass
    [Documentation]    This is a simple test to show how Robot can be used to run command line argumets from the OS
    [Tags]    demo
    ${RESULT}    Run    ping www.google.com
    ShouldContain    ${RESULT}    0%
    set suite metadata  ping_result    ${RESULT}