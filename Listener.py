__author__ = 'iwelch'
"""The Listener defines all methods that RobotFramework will call out to as is loads and executes test suites
"""
import logging as logger

ROBOT_LISTENER_API_VERSION = 3

logger.debug("Starting Test Monitor")
tests = []
suite_loaded = False

def end_suite(name, attributes):
    for foo in tests:
        print foo.name
        for result in foo.results:
            print ("\t{}\t{}\t{}".format(result['result'], result['execution_time'], result['duration']))

def start_suite(name, attributes):
    global tests, suite_loaded
    logger.debug("Staring the test suite")
    if suite_loaded:
        return
    for test in name.tests:
        tests.append(test_container(test.name, test.id))
    suite_loaded = True

def end_test(data, result):
    global tests
    for test in tests:
        if test.id == data.id:
            test.add_result(result.status, result.starttime, result.elapsedtime)
    print 'Test: %s ran in %s ms and resulted in a %s' % (result.name, result.elapsedtime, result.status)

#def log_message(message):
    #print message

class test_container:
    #TODO should this just inherit a test object from RobotFramework and add additional variables / methods?
    #TODO add a class variables / objects / methods for UI objects
    def __init__(self, name, id):
        self.name = name
        self.id = id
        self.results = []
        pass

    def add_result(self, result, time, duration):
        my_result = {'result': result, 'execution_time': time, 'duration': duration}
        self.results.append(my_result)

    def get_results(self):
        return self.results