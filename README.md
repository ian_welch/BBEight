# Introduction
BBEight is a skittish but loyal astromech who accompanied Poe Dameron on many missions for the Resistance..... ok it's 
not that. BBEight is a static wrapper around RobotFramework that allows RobotFramework test suites to be run in a loop
or in a read only UI. It was developed as a tool to allow test engineers to easily write tests in RobotFramework and use 
them in factory, environmental testing, or any other testing in which the user may require a UI, they may need to run
tests continually or you don't want to allow the user to modify the tests but only write them. By writing tests in 
RobotFramework the same test scripts can be used by engineering development teams to also test the device thereby 
allowing for a common framework to be used across engineering, testing, and the factory. In addition to a common test
framework a common test platform (hardware required to run the test) can also be leverages such that the hardware 
engineering is using to develop and test new features is the same hardware platform being used by other test and factory 
groups.

# Getting Started
## Dependencies
- [Python 2.7](https://www.python.org/downloads)
- [RobotFramework](www.RobotFramework.org) 

## Installation
1. Download and install Python following the on screen instruction
2. Install RobotFramework using python command line `pip install robotframework` or `python -m pip install robotframework`
3. Install PyQt4 using the python command line `pip install PyQt4` or `python -m pip install PyQt4`. Alternatily you can 
download and install the package from [Riverbank](https://www.riverbankcomputing.com/software/pyqt/download5)
4. Install BBEight _maybe we use gist?_

## Configuration
 TODO